package com.starplan.index.entity;

/**
 * <p>User: jaylee
 * <p>Date: 2015-10-26 上午9:49
 * <p>Version: 1.0
 */
public enum ArrangementType {
    unify("统一"), one("区域"), area("挨个");

    private final String info;

    private ArrangementType(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }
}
