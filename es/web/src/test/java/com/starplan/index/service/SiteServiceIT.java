package com.starplan.index.service;

import com.sishuok.es.test.BaseIT;
import com.starplan.index.entity.StarplanSite;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;

import java.util.Date;

/**
 * <p>User: jaylee
 * <p>Date: 2015-10-26 下午4:26
 * <p>Version: 1.0
 */
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class SiteServiceIT extends BaseIT {
    @Autowired
    private StarplanSiteService starplanSiteService;

    @Test
    public void testCount() {
        long count = starplanSiteService.count();
        Assert.assertEquals(3, count);
    }

    protected StarplanSite createSite(){
        StarplanSite starplanSite = new StarplanSite();
        return starplanSite;
    }
}
