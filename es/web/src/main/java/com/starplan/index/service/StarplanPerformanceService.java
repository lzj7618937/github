package com.starplan.index.service;

import com.sishuok.es.common.service.BaseService;


import com.starplan.index.entity.StarplanPerformance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 * starplan_performance 服务类
 * <p>User: jaylee
 * <p>Date: Tue Oct 27 07:50:48 CST 2015
 * <p>Version: 1.0
 */
@Service
public class StarplanPerformanceService extends BaseService<StarplanPerformance, Long> {

    //performance每页返回的数据量
    private int pageCount = 10;

    //按hot顺序返回前10个performance
    public Page<StarplanPerformance> getPerformances(int pageNum, String orderStr) {
        Sort sortAsc = new Sort(Sort.Direction.ASC, orderStr);
        int start = pageCount * (pageNum - 1);
        Pageable pageable = new PageRequest(start, pageCount, sortAsc);
        return findAll(pageable);
    }


}

