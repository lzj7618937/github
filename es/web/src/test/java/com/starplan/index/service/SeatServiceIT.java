package com.starplan.index.service;

import com.sishuok.es.test.BaseIT;
import com.starplan.index.entity.ArrangementState;
import com.starplan.index.entity.ArrangementType;
import com.starplan.index.entity.StarplanArrangement;
import com.starplan.index.entity.StarplanSeat;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;

import java.util.Date;

/**
 * <p>User: jaylee
 * <p>Date: 2015-10-26 下午4:26
 * <p>Version: 1.0
 */
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class SeatServiceIT extends BaseIT {
    @Autowired
    private StarplanSeatService starplanSeatService;

    @Test
    public void testCount() {
        long count = starplanSeatService.count();
        Assert.assertEquals(1000, count);
    }

    protected StarplanSeat createSeat(){
        StarplanSeat starplanSeat = new StarplanSeat();
        return starplanSeat;
    }

}
