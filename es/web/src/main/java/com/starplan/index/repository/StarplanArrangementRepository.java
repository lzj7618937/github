package com.starplan.index.repository;

import com.sishuok.es.common.repository.BaseRepository;
import com.starplan.index.entity.StarplanArrangement;


/**
* starplan_arrangement 数据操作类 
* <p>User: jaylee 
* <p>Date: Mon Oct 26 09:46:38 CST 2015 
* <p>Version: 1.0 
*/ 
public interface StarplanArrangementRepository extends BaseRepository<StarplanArrangement, Long>{
}

