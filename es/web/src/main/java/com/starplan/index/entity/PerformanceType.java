package com.starplan.index.entity;

/**
 * <p>User: jaylee
 * <p>Date: 2015-10-27 上午8:05
 * <p>Version: 1.0
 */
public enum PerformanceType {
    drama("话剧"), concert("演唱会"), football("足球比赛"), other("其它");

    private final String info;

    private PerformanceType(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }
}
