package com.starplan.index.entity;

import com.sishuok.es.common.entity.BaseEntity;

import javax.persistence.*;

/**
* starplan_seat 实体类 
* <p>User: jaylee 
* <p>Date: Tue Oct 27 22:00:32 CST 2015 
* <p>Version: 1.0 
*/ 

@Entity 
@Table(name = "starplan_seat")
public class StarplanSeat extends BaseEntity<Long>{

	@Column(name = "siteId")
	private int siteId;

	@Column(name = "area")
	private String area;

	@Column(name = "row")
	private String row;

	@Column(name = "col")
	private String col;

	@Column(name = "isFixed")
	private boolean isFixed;

	@Column(name = "remark")
	private String remark;

	public void setSiteId(int siteId){
		this.siteId = siteId;
	}

	public int getSiteId(){
		return siteId;
	}

	public void setArea(String area){
		this.area = area;
	}

	public String getArea(){
		return area;
	}

	public void setRow(String row){
		this.row = row;
	}

	public String getRow(){
		return row;
	}

	public void setCol(String col){
		this.col = col;
	}

	public String getCol(){
		return col;
	}

	public void setIsFixed(boolean isFixed){
		this.isFixed = isFixed;
	}

	public boolean getIsFixed(){
		return isFixed;
	}

	public void setRemark(String remark){
		this.remark = remark;
	}

	public String getRemark(){
		return remark;
	}
}

