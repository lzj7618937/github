DROP PROCEDURE IF EXISTS `insert_performance`;
DROP PROCEDURE IF EXISTS `insert_arrangement`;
DROP PROCEDURE IF EXISTS `insert_site`;
DROP PROCEDURE IF EXISTS `insert_seat`;
DROP PROCEDURE IF EXISTS `insert_ticket`;
DROP PROCEDURE IF EXISTS `insert_order`;

DELIMITER @
CREATE PROCEDURE insert_performance(IN item INTEGER)
  BEGIN

    #要造多少数据
    DECLARE counter INT;
    #演出标题
    DECLARE title VARCHAR(100) DEFAULT '标题_TITLE_TEST';
    #分类归属
    DECLARE type VARCHAR(50) DEFAULT '演唱会';
    #演出简介
    DECLARE introduce VARCHAR(100) DEFAULT '<h1>演出简介_TITLE_TEST</h1>';
    #演出详细介绍
    DECLARE detail VARCHAR(1000) DEFAULT '<h2>演出详细介绍_DETAIL_TEST</h2>';
    #演出简介
    DECLARE introducep VARCHAR(100) DEFAULT '<h1>演出简介_TITLE_TEST</h1>';
    #演出详细介绍
    DECLARE detailp VARCHAR(1000) DEFAULT '<h2>演出详细介绍_DETAIL_TEST</h2>';
    #缩略图路径
    DECLARE thumb VARCHAR(100) DEFAULT '/static/images/index/jianoufenge.jpg';
    #背景大图路径
    DECLARE backgroud VARCHAR(100) DEFAULT '/static/images/index/jianoufenge.jpg';
    #热度
    DECLARE hot INT DEFAULT 0;

    TRUNCATE TABLE `starplan_performance`;

    SET counter = 10;

    WHILE counter >= 1 DO

      #随机设置热度值
      SET hot = floor(1 + rand() * 5);

      #随机设置演出类型
      CASE counter % 4
        WHEN 0
        THEN
          SET type = '话剧';
        WHEN 1
        THEN
          SET type = '演唱会';
        WHEN 2
        THEN
          SET type = '其它';
      ELSE
        SET type = '足球比赛';
      END CASE;

      SET introducep = REPEAT('<p>很好看的演出内容INTRODUCE</p>', 1);
      SET detailp = REPEAT('<p>很好看的演出内容DETAIL</p>', 5);

      SET introduce = concat('<h1>演出简介_TITLE_TEST</h1>', introducep);
      SET detail = concat('<h2>演出详细介绍_DETAIL_TEST</h2>', detailp);

      SET title = concat('标题_TITLE_TEST', counter);

      INSERT INTO `es`.`starplan_performance` (
        `id`,
        `title`,
        `type`,
        `introduce`,
        `detail`,
        `thumb`,
        `backgroud`,
        `hot`
      )
      VALUES (counter, title, type, introduce, detail, thumb, backgroud, hot);

      CALL insert_arrangement(counter);
      SET counter = counter - 1;

    END WHILE;

  END
@


CREATE PROCEDURE insert_arrangement(IN pfmcId INTEGER)
  BEGIN

    #要造多少数据
    DECLARE counter INT;
    #演出标题
    DECLARE title VARCHAR(100) DEFAULT '标题_TITLE_TEST';
    #分类归属
    DECLARE type VARCHAR(50) DEFAULT '统一';
    #剩余数量
    DECLARE num INT DEFAULT 0;
    #演出开始时间
    DECLARE beginDate DATE DEFAULT sysdate();
    #演出结束时间
    DECLARE endDate DATE DEFAULT sysdate();
    #演出售票状态
    DECLARE state VARCHAR(50) DEFAULT '待售';
    #演出简介
    DECLARE introduce VARCHAR(100) DEFAULT '<h1>演出简介_TITLE_TEST</h1>';
    #演出详细介绍
    DECLARE detail VARCHAR(1000) DEFAULT '<h2>演出详细介绍_DETAIL_TEST</h2>';
    #演出简介
    DECLARE introducep VARCHAR(100) DEFAULT '<h1>演出简介_TITLE_TEST</h1>';
    #演出详细介绍
    DECLARE detailp VARCHAR(1000) DEFAULT '<h2>演出详细介绍_DETAIL_TEST</h2>';
    #缩略图路径
    DECLARE thumb VARCHAR(100) DEFAULT '/static/images/index/jianoufenge.jpg';
    #背景大图路径
    DECLARE backgroud VARCHAR(100) DEFAULT '/static/images/index/jianoufenge.jpg';
    #热度
    DECLARE hot INT DEFAULT 0;
    #是否放置至广告处
    DECLARE isAdv INT DEFAULT 0;

    DECLARE tempInt INT DEFAULT 0;
    DECLARE tempStr VARCHAR(100) DEFAULT '';

    DELETE FROM `es`.`starplan_arrangement`
    WHERE `perfId` = pfmcId;

    #每场演出有多少排场安排
    SET counter = floor(1 + rand() * 5);

    WHILE counter >= 1 DO

      #随机设置热度值
      SET hot = floor(1 + rand() * 5);

      SET tempInt = floor(1 + rand() * 5);

      #随机设置演出类型
      CASE counter % 3
        WHEN 0
        THEN
          SET type = '话剧';
        WHEN 1
        THEN
          SET type = '演唱会';
        WHEN 2
        THEN
          SET type = '其它';
      ELSE
        SET type = '足球比赛';
      END CASE;

      SET beginDate = DATE_ADD(beginDate, INTERVAL tempInt DAY);
      SET endDate = DATE_ADD(endDate, INTERVAL tempInt DAY);

      SET introducep = repeat('<p>很好看的演出内容INTRODUCE</p>', 10);
      SET detailp = REPEAT('<p>很好看的演出内容DETAIL</p>', 50);

      SET introduce = concat('<h1>演出简介_TITLE_TEST</h1>', introducep);
      SET detail = concat('<h2>演出详细介绍_DETAIL_TEST</h2>', detailp);
      SET tempStr = concat(counter, '');

      INSERT INTO `es`.`starplan_arrangement` (`id`, `perfId`, `siteId`, `title`, `type`, `num`, `beginDate`, `endDate`, `state`, `introduce`, `detail`,
                                               `thumb`, `backgroud`, `isAdv`, `hot`)
      VALUES
        (NULL, pfmcId, '0', title, type, num, beginDate, endDate, state, introduce, detail,
         thumb, backgroud, isAdv, hot);

      SET counter = counter - 1;

    END WHILE;

  END
@

CREATE PROCEDURE insert_site(IN item INTEGER)
  BEGIN

    #要造多少数据
    DECLARE counter INT;
    DECLARE id INT;
    #演出标题
    DECLARE title VARCHAR(100) DEFAULT '标题_TITLE_TEST';
    #分类归属
    DECLARE type VARCHAR(50) DEFAULT '体育场';
    #演出简介
    DECLARE introduce VARCHAR(100) DEFAULT '场馆简介_TEST';
    #地址
    DECLARE address VARCHAR(1000) DEFAULT '地址';
    #联系方式
    DECLARE contact VARCHAR(100) DEFAULT '021-68893450';
    #缩略图路径
    DECLARE thumb VARCHAR(100) DEFAULT '/static/images/index/jianoufenge.jpg';
    #背景大图路径
    DECLARE backgroud VARCHAR(100) DEFAULT '/static/images/index/jianoufenge.jpg';
    #热度
    DECLARE hot INT DEFAULT 0;

    TRUNCATE TABLE starplan_site;

    INSERT INTO `es`.`starplan_site` (`id`, `title`, `type`, `address`, `contact`, `introduce`, `thumb`, `backgroud`, `hot`)
    VALUES ('1', '暂时未设定场馆', '其它', '暂时未设定场馆', '暂时未设定场馆', '暂时未设定场馆', '暂时未设定场馆', '暂时未设定场馆', '0');

    SET counter = item;

    WHILE counter >= 1 DO

      #随机设置热度值
      SET hot = floor(1 + rand() * 5);

      #随机设置演出类型
      CASE counter % 4
        WHEN 0
        THEN
          SET type = '体育场';
        WHEN 1
        THEN
          SET type = '会展中心';
        WHEN 2
        THEN
          SET type = '酒吧';
      ELSE
        SET type = '其它';
      END CASE;

      SET introduce = concat('场馆简介_TEST', counter);

      SET title = concat('标题_TITLE_TEST', counter);

      SET id = counter + 1;

      INSERT INTO `es`.`starplan_site` (`id`, `title`, `type`, `address`, `contact`, `introduce`, `thumb`, `backgroud`, `hot`)
      VALUES (id, title, type, address, contact, introduce, thumb, backgroud, hot);

      CALL insert_seat(id);

      SET counter = counter - 1;

    END WHILE;

  END
@

CREATE PROCEDURE insert_seat(IN siteId INTEGER)
  BEGIN
    DECLARE counter INT;
    DECLARE row INT;
    DECLARE col INT;
    DECLARE area VARCHAR(10);

    DELETE FROM `es`.`starplan_seat`
    WHERE `siteId` = siteId;

    SET counter = 1000;
    WHILE counter >= 1 DO
      #演出区域
      CASE counter % 4
        WHEN 0
        THEN
          SET area = 'A';
        WHEN 1
        THEN
          SET area = 'B';
        WHEN 2
        THEN
          SET area = 'C';
      ELSE
        SET area = 'D';
      END CASE;
      SET row = floor(counter / 100);
      SET col = counter % 100;
      INSERT INTO `es`.`starplan_seat` (`id`, `siteId`, `area`, `row`, `col`, `isFixed`, `remark`)
      VALUES (NULL, siteId, area, row, col, '1', 'fsaf');
      SET counter = counter - 1;
    END WHILE;
  END
@

CREATE PROCEDURE insert_ticket(IN item INTEGER)
  BEGIN
    DECLARE counter INT;
    SET counter = item;
    WHILE counter >= 1 DO
      INSERT INTO innodb VALUES (counter, concat('mysqlsystems.com', counter), repeat('bla', 10));
      SET counter = counter - 1;
    END WHILE;
  END
@

CREATE PROCEDURE insert_order(IN item INTEGER)
  BEGIN
    DECLARE counter INT;
    SET counter = item;
    WHILE counter >= 1 DO
      INSERT INTO innodb VALUES (counter, concat('mysqlsystems.com', counter), repeat('bla', 10));
      SET counter = counter - 1;
    END WHILE;
  END
@

DELIMITER ;