package com.starplan.index.repository;

import com.sishuok.es.common.repository.BaseRepository;
import com.starplan.index.entity.StarplanOrder;

/**
* starplan_order 数据操作类 
* <p>User: jaylee 
* <p>Date: Tue Oct 27 22:18:44 CST 2015 
* <p>Version: 1.0 
*/ 
public interface StarplanOrderRepository extends BaseRepository<StarplanOrder, Long>{
}

