package com.starplan.index.service;

import com.sishuok.es.test.BaseIT;
import com.starplan.index.entity.ArrangementState;
import com.starplan.index.entity.ArrangementType;
import com.starplan.index.entity.StarplanArrangement;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;

import java.util.Date;

/**
 * <p>User: jaylee
 * <p>Date: 2015-10-26 下午4:26
 * <p>Version: 1.0
 */
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class ArrangementServiceIT extends BaseIT {
    @Autowired
    private StarplanArrangementService starplanArrangementService;

    @Test
    public void testCount() {
        long count = starplanArrangementService.count();
        Assert.assertEquals(35, count);
    }

    @Test
    public void testSave() {
        long count = starplanArrangementService.count();
        long id  = starplanArrangementService.save(createArrangement()).getId();
        Assert.assertEquals(starplanArrangementService.count(), count + 1);
        starplanArrangementService.delete(id);
        Assert.assertEquals(starplanArrangementService.count(),count);
    }

    protected StarplanArrangement createArrangement() {
        StarplanArrangement starplanArrangement = new StarplanArrangement();

        int perfId = 2;
        int siteId = 2;
        String title = "titleFromWeb2";
        ArrangementType type = ArrangementType.area;
        int num = 160;
        Date beginDate = new Date();
        Date endDate = new Date();
        ArrangementState state = ArrangementState.sell;
        String introduce = "introduce from WEB2";
        String detail = "detail from WEB2";
        String thumb = "thumb from WEB2";
        String backgroud = "backgroud from WEB2";
        boolean isAdv = true;
        short hot = 10;

        starplanArrangement.setPerfId(perfId);
        starplanArrangement.setSiteId(siteId);
        starplanArrangement.setTitle(title);
        starplanArrangement.setType(type);
        starplanArrangement.setNum(num);
        starplanArrangement.setBeginDate(beginDate);
        starplanArrangement.setEndDate(endDate);
        starplanArrangement.setState(state);
        starplanArrangement.setIntroduce(introduce);
        starplanArrangement.setDetail(detail);
        starplanArrangement.setThumb(thumb);
        starplanArrangement.setBackgroud(backgroud);
        starplanArrangement.setIsAdv(isAdv);
        starplanArrangement.setHot(hot);
        return starplanArrangement;
    }

}
