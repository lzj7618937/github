package com.starplan.index.service;

import com.sishuok.es.test.BaseIT;
import com.starplan.index.entity.StarplanTicket;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;

import java.util.Date;

/**
 * <p>User: jaylee
 * <p>Date: 2015-10-26 下午4:26
 * <p>Version: 1.0
 */
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class TicketServiceIT extends BaseIT {
    @Autowired
    private StarplanTicketService starplanTicketService;

    @Test
    public void testCount() {
        long count = starplanTicketService.count();
        Assert.assertEquals(0, count);
    }

    public StarplanTicket createTicket(){
        StarplanTicket starplanTicket = new StarplanTicket();
        return starplanTicket;
    }
}
