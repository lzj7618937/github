package com.starplan.index.service;

import com.sishuok.es.common.service.BaseService;


import com.starplan.index.entity.StarplanSite;
import org.springframework.stereotype.Service;

/**
* starplan_site 服务类 
* <p>User: jaylee 
* <p>Date: Tue Oct 27 21:59:55 CST 2015 
* <p>Version: 1.0 
*/ 
@Service
public class StarplanSiteService extends BaseService<StarplanSite, Long> {
}

