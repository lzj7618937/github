package com.sishuok.es.index.repository;

import com.sishuok.es.common.repository.BaseRepository;
import com.sishuok.es.index.entity.IndexCategory;


/**
* index_category 数据操作类 
* <p>User: jaylee 
* <p>Date: Tue Oct 06 23:11:26 CST 2015 
* <p>Version: 1.0 
*/ 
public interface IndexCategoryRepository extends BaseRepository<IndexCategory, Long>{
}

