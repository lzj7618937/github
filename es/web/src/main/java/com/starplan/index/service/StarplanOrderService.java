package com.starplan.index.service;

import com.sishuok.es.common.service.BaseService;

import com.starplan.index.entity.StarplanOrder;
import org.springframework.stereotype.Service;

/**
* starplan_order 服务类 
* <p>User: jaylee 
* <p>Date: Tue Oct 27 22:18:44 CST 2015 
* <p>Version: 1.0 
*/ 
@Service
public class StarplanOrderService extends BaseService<StarplanOrder, Long> {
}

