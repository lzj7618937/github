package com.starplan.index.repository;

import com.sishuok.es.common.repository.BaseRepository;
import com.starplan.index.entity.StarplanPerformance;


/**
* starplan_performance 数据操作类 
* <p>User: jaylee 
* <p>Date: Tue Oct 27 07:50:48 CST 2015 
* <p>Version: 1.0 
*/ 
public interface StarplanPerformanceRepository extends BaseRepository<StarplanPerformance, Long>{
}

