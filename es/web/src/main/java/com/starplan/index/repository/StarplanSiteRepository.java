package com.starplan.index.repository;

import com.sishuok.es.common.repository.BaseRepository;
import com.starplan.index.entity.StarplanSite;


/**
* starplan_site 数据操作类 
* <p>User: jaylee 
* <p>Date: Tue Oct 27 21:59:55 CST 2015 
* <p>Version: 1.0 
*/ 
public interface StarplanSiteRepository extends BaseRepository<StarplanSite, Long>{
}

