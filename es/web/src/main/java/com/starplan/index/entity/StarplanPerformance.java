package com.starplan.index.entity;

import com.sishuok.es.common.entity.BaseEntity;

import javax.persistence.*;

/**
* starplan_performance 实体类 
* <p>User: jaylee 
* <p>Date: Tue Oct 27 07:50:48 CST 2015 
* <p>Version: 1.0 
*/ 

@Entity 
@Table(name = "starplan_performance")
public class StarplanPerformance extends BaseEntity<Long>{

	@Column(name = "title")
	private String title;

	@Column(name = "type")
	@Enumerated(value = EnumType.STRING)
	private PerformanceType type;

	@Column(name = "introduce")
	private String introduce;

	@Column(name = "detail")
	private String detail;

	@Column(name = "thumb")
	private String thumb;

	@Column(name = "backgroud")
	private String backgroud;

	@Column(name = "hot")
	private short hot;

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setType(PerformanceType type){
		this.type = type;
	}

	public PerformanceType getType(){
		return type;
	}

	public void setIntroduce(String introduce){
		this.introduce = introduce;
	}

	public String getIntroduce(){
		return introduce;
	}

	public void setDetail(String detail){
		this.detail = detail;
	}

	public String getDetail(){
		return detail;
	}

	public void setThumb(String thumb){
		this.thumb = thumb;
	}

	public String getThumb(){
		return thumb;
	}

	public void setBackgroud(String backgroud){
		this.backgroud = backgroud;
	}

	public String getBackgroud(){
		return backgroud;
	}

	public void setHot(short hot){
		this.hot = hot;
	}

	public short getHot(){
		return hot;
	}
}

