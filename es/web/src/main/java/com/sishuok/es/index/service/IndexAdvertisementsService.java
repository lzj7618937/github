package com.sishuok.es.index.service;

import com.google.common.collect.Maps;
import com.sishuok.es.common.entity.search.Searchable;
import com.sishuok.es.common.service.BaseService;
import com.sishuok.es.index.entity.IndexAdvertisements;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
* index_advertisements 服务类 
* <p>User: jaylee 
* <p>Date: Mon Sep 28 22:34:01 CST 2015 
* <p>Version: 1.0 
*/ 
@Service
public class IndexAdvertisementsService extends BaseService<IndexAdvertisements, Long> {

    public List<IndexAdvertisements> getMostPopAdv(int num){
        List<IndexAdvertisements> indexAdvertisementsList = new ArrayList<IndexAdvertisements>();
        Pageable pageable = new PageRequest(0, num);

        Map<String, Object> searchParams = Maps.newHashMap();
        searchParams.put("available_eq", true);
        Sort sort = new Sort(Sort.Direction.ASC, "displayorder");
        Page<IndexAdvertisements> page = findAll(Searchable.newSearchable(searchParams).addSort(sort).setPage(pageable));

        if (page.hasContent()) {
            return page.getContent();
        }
        return null;
    }
}

