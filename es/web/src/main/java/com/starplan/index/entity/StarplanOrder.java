package com.starplan.index.entity;

import com.sishuok.es.common.entity.BaseEntity;

import javax.persistence.*;

/**
* starplan_order 实体类 
* <p>User: jaylee 
* <p>Date: Tue Oct 27 22:18:44 CST 2015 
* <p>Version: 1.0 
*/ 

@Entity 
@Table(name = "starplan_order")
public class StarplanOrder extends BaseEntity<Long>{

	@Column(name = "ticketId")
	private int ticketId;

	@Column(name = "num")
	private int num;

	@Column(name = "price")
	private int price;

	@Column(name = "name")
	private String name;

	@Column(name = "contact")
	private String contact;

	@Column(name = "address")
	private String address;

	@Column(name = "deliver")
	private String deliver;

	@Column(name = "remark")
	private String remark;

	public void setTicketId(int ticketId){
		this.ticketId = ticketId;
	}

	public int getTicketId(){
		return ticketId;
	}

	public void setNum(int num){
		this.num = num;
	}

	public int getNum(){
		return num;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setContact(String contact){
		this.contact = contact;
	}

	public String getContact(){
		return contact;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setDeliver(String deliver){
		this.deliver = deliver;
	}

	public String getDeliver(){
		return deliver;
	}

	public void setRemark(String remark){
		this.remark = remark;
	}

	public String getRemark(){
		return remark;
	}
}

