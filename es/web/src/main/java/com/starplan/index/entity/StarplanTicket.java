package com.starplan.index.entity;

import com.sishuok.es.common.entity.BaseEntity;

import javax.persistence.*;

/**
* starplan_ticket 实体类 
* <p>User: jaylee 
* <p>Date: Tue Oct 27 22:13:11 CST 2015 
* <p>Version: 1.0 
*/ 

@Entity 
@Table(name = "starplan_ticket")
public class StarplanTicket extends BaseEntity<Long>{

	@Column(name = "seatId")
	private int seatId;

	@Column(name = "argmtId")
	private int argmtId;

	@Column(name = "price")
	private String price;

	@Column(name = "state")
	@Enumerated(value = EnumType.STRING)
	private TicketState state;

	@Column(name = "remark")
	private String remark;

	public void setSeatId(int seatId){
		this.seatId = seatId;
	}

	public int getSeatId(){
		return seatId;
	}

	public void setArgmtId(int argmtId){
		this.argmtId = argmtId;
	}

	public int getArgmtId(){
		return argmtId;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setState(TicketState state){
		this.state = state;
	}

	public TicketState getState(){
		return state;
	}

	public void setRemark(String remark){
		this.remark = remark;
	}

	public String getRemark(){
		return remark;
	}
}

