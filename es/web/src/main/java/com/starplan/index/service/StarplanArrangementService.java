package com.starplan.index.service;

import com.sishuok.es.common.service.BaseService;


import com.starplan.index.entity.StarplanArrangement;
import org.springframework.stereotype.Service;

/**
* starplan_arrangement 服务类 
* <p>User: jaylee 
* <p>Date: Mon Oct 26 09:46:38 CST 2015 
* <p>Version: 1.0 
*/ 
@Service
public class StarplanArrangementService extends BaseService<StarplanArrangement, Long> {

}

