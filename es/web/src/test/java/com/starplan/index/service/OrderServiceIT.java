package com.starplan.index.service;

import com.sishuok.es.test.BaseIT;
import com.starplan.index.entity.StarplanOrder;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;

import java.util.Date;

/**
 * <p>User: jaylee
 * <p>Date: 2015-10-26 下午4:26
 * <p>Version: 1.0
 */
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class OrderServiceIT extends BaseIT {
    @Autowired
    private StarplanOrderService starplanOrderService; 

    @Test
    public void testCount() {
        long count = starplanOrderService.count();
        Assert.assertEquals(0, count);
    }

    protected StarplanOrder createOrder(){
        StarplanOrder starplanOrder = new StarplanOrder();
        return starplanOrder;
    }
}
