package com.starplan.index.service;

import com.sishuok.es.common.service.BaseService;


import com.starplan.index.entity.StarplanSeat;
import org.springframework.stereotype.Service;

/**
* starplan_seat 服务类 
* <p>User: jaylee 
* <p>Date: Tue Oct 27 22:00:32 CST 2015 
* <p>Version: 1.0 
*/ 
@Service
public class StarplanSeatService extends BaseService<StarplanSeat, Long> {
}

