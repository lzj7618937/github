package com.starplan.index.repository;

import com.sishuok.es.common.repository.BaseRepository;
import com.starplan.index.entity.StarplanSeat;


/**
* starplan_seat 数据操作类 
* <p>User: jaylee 
* <p>Date: Tue Oct 27 22:00:32 CST 2015 
* <p>Version: 1.0 
*/ 
public interface StarplanSeatRepository extends BaseRepository<StarplanSeat, Long>{
}

