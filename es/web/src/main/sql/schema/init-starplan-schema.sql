#如果复制到mysql中执行时 加上
#DELIMITER ;;

-- --------------------------------------------------------

--
-- 表的结构 `starplan_arrangement`
--

DROP TABLE IF EXISTS `starplan_arrangement`;
CREATE TABLE IF NOT EXISTS `starplan_arrangement` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，编号，自增',
  `perfId` int(11) NOT NULL COMMENT '演出信息ID',
  `siteId` int(11) NOT NULL DEFAULT '0' COMMENT '场管ID',
  `title` varchar(100) NOT NULL COMMENT '演出标题',
  `type` VARCHAR(50) NOT NULL DEFAULT '统一' COMMENT '定价方式:''统一'',''区域'',''挨个''',
  `num` int(11) NOT NULL DEFAULT '0' COMMENT '剩余数量',
  `beginDate` datetime DEFAULT NULL COMMENT '演出开始时间',
  `endDate` datetime DEFAULT NULL COMMENT '演出结束时间',
  `state` VARCHAR(50) NOT NULL DEFAULT '待售' COMMENT '演出售票状态:''售中'',''售完'',''待售''',
  `introduce` varchar(1000) DEFAULT NULL COMMENT '演出简介',
  `detail` text COMMENT '详情',
  `thumb` varchar(100) DEFAULT NULL COMMENT '缩略图路径',
  `backgroud` varchar(100) DEFAULT NULL COMMENT '背景大图',
  `isAdv` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否放置至广告处',
  `hot` smallint(3) NOT NULL DEFAULT '0' COMMENT '热度',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='场次信息表：记录场次安排信息。' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `starplan_order`
--

DROP TABLE IF EXISTS `starplan_order`;
CREATE TABLE IF NOT EXISTS `starplan_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，编号，自增',
  `ticketId` int(11) NOT NULL COMMENT '门票ID',
  `num` int(11) NOT NULL DEFAULT '1' COMMENT '购买数量',
  `price` int(11) NOT NULL DEFAULT '0' COMMENT '实际购买价格',
  `name` varchar(100) NOT NULL COMMENT '收货人姓名',
  `contact` varchar(100) NOT NULL COMMENT '联系方式',
  `address` varchar(100) NOT NULL COMMENT '详细地址',
  `deliver` enum('平邮','EMS','自提') NOT NULL DEFAULT '平邮' COMMENT '快递方式',
  `remark` varchar(1000) NOT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单表：记录用户订单信息。' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `starplan_performance`
--

DROP TABLE IF EXISTS `starplan_performance`;
CREATE TABLE IF NOT EXISTS `starplan_performance` (
  `id` tinyint(6) NOT NULL AUTO_INCREMENT COMMENT '主键，编号，自增',
  `title` varchar(100) NOT NULL COMMENT '演出标题',
  `type` VARCHAR(50) NOT NULL DEFAULT '演唱会' COMMENT '分类归属:''话剧'',''演唱会'',''足球比赛'',''其它''',
  `introduce` varchar(1000) DEFAULT NULL COMMENT '演出简介',
  `detail` text COMMENT '演出详细介绍',
  `thumb` varchar(100) DEFAULT NULL COMMENT '缩略图路径',
  `backgroud` varchar(100) DEFAULT NULL COMMENT '背景大图',
  `hot` smallint(3) NOT NULL DEFAULT '0' COMMENT '热度',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='演出信息表：记录演出信息。' AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- 表的结构 `starplan_seat`
--

DROP TABLE IF EXISTS `starplan_seat`;
CREATE TABLE IF NOT EXISTS `starplan_seat` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，编号，自增',
  `siteId` int(11) NOT NULL COMMENT '场馆ID',
  `area` varchar(100) NOT NULL DEFAULT 'A' COMMENT '区域',
  `row` varchar(100) NOT NULL DEFAULT '0' COMMENT '座位所在排数',
  `col` varchar(100) NOT NULL DEFAULT '0' COMMENT '座位所在列数',
  `isFixed` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否固定',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='座位信息表：记录场馆座位信息。' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `starplan_site`
--

DROP TABLE IF EXISTS `starplan_site`;
CREATE TABLE IF NOT EXISTS `starplan_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，编号，自增',
  `title` varchar(100) NOT NULL COMMENT '场馆标题',
  `type` enum('体育场','会展中心','酒吧','其它') NOT NULL DEFAULT '体育场' COMMENT '场馆分类',
  `address` varchar(1000) DEFAULT NULL COMMENT '场馆地址',
  `contact` varchar(100) DEFAULT NULL COMMENT '联系方式',
  `introduce` text COMMENT '场馆介绍',
  `thumb` varchar(100) DEFAULT NULL COMMENT '缩略图路径',
  `backgroud` varchar(100) DEFAULT NULL COMMENT '背景大图',
  `hot` smallint(3) NOT NULL DEFAULT '0' COMMENT '热度',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='场馆信息表：记录场馆信息' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `starplan_ticket`
--

DROP TABLE IF EXISTS `starplan_ticket`;
CREATE TABLE IF NOT EXISTS `starplan_ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，编号，自增',
  `seatId` int(11) NOT NULL COMMENT '座位ID',
  `argmtId` int(11) NOT NULL COMMENT '场次ID',
  `price` varchar(100) DEFAULT NULL COMMENT '价格',
  `state` enum('已售','待售') NOT NULL DEFAULT '待售' COMMENT '状态（已/未售）',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='门票信息表：记录演出门票信息。' AUTO_INCREMENT=1 ;


