package com.sishuok.es.index.entity;

import com.sishuok.es.common.entity.BaseEntity;

import javax.persistence.*;

/**
* index_category 实体类 
* <p>User: jaylee 
* <p>Date: Tue Oct 06 23:11:26 CST 2015 
* <p>Version: 1.0 
*/ 

@Entity 
@Table(name = "index_category")
public class IndexCategory extends BaseEntity<Long>{

	@Column(name = "name")
	private String name;

	@Column(name = "hot")
	private byte hot;

	@Column(name = "img")
	private String img;

	@Column(name = "comments")
	private String comments;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setHot(byte hot){
		this.hot = hot;
	}

	public byte getHot(){
		return hot;
	}

	public void setImg(String img){
		this.img = img;
	}

	public String getImg(){
		return img;
	}

	public void setComments(String comments){
		this.comments = comments;
	}

	public String getComments(){
		return comments;
	}
}

