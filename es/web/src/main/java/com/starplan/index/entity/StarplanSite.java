package com.starplan.index.entity;

import com.sishuok.es.common.entity.BaseEntity;

import javax.persistence.*;

/**
* starplan_site 实体类 
* <p>User: jaylee 
* <p>Date: Tue Oct 27 21:59:55 CST 2015 
* <p>Version: 1.0 
*/ 

@Entity 
@Table(name = "starplan_site")
public class StarplanSite extends BaseEntity<Long>{

	@Column(name = "title")
	private String title;

	@Column(name = "type")
	@Enumerated(value = EnumType.STRING)
	private SiteType type;

	@Column(name = "address")
	private String address;

	@Column(name = "contact")
	private String contact;

	@Column(name = "introduce")
	private String introduce;

	@Column(name = "thumb")
	private String thumb;

	@Column(name = "backgroud")
	private String backgroud;

	@Column(name = "hot")
	private short hot;

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setType(SiteType type){
		this.type = type;
	}

	public SiteType getType(){
		return type;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setContact(String contact){
		this.contact = contact;
	}

	public String getContact(){
		return contact;
	}

	public void setIntroduce(String introduce){
		this.introduce = introduce;
	}

	public String getIntroduce(){
		return introduce;
	}

	public void setThumb(String thumb){
		this.thumb = thumb;
	}

	public String getThumb(){
		return thumb;
	}

	public void setBackgroud(String backgroud){
		this.backgroud = backgroud;
	}

	public String getBackgroud(){
		return backgroud;
	}

	public void setHot(short hot){
		this.hot = hot;
	}

	public short getHot(){
		return hot;
	}
}

