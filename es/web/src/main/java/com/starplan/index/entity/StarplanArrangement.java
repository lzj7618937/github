package com.starplan.index.entity;

import java.util.Date;

import com.sishuok.es.common.entity.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;

/**
 * starplan_arrangement 实体类
 * <p>User: jaylee
 * <p>Date: Mon Oct 26 09:46:38 CST 2015
 * <p>Version: 1.0
 */

@Entity
@Table(name = "starplan_arrangement")
public class StarplanArrangement extends BaseEntity<Long> {

    @Column(name = "perfId")
    private int perfId;

    @Column(name = "siteId")
    private int siteId;

    @Column(name = "title")
    private String title;

    @Column(name = "type")
    @Enumerated(value = EnumType.STRING)
    private ArrangementType type;

    @Column(name = "num")
    private int num;

    @Column(name = "beginDate")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    private Date beginDate;

    @Column(name = "endDate")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @Column(name = "state")
    @Enumerated(value = EnumType.STRING)
    private ArrangementState state;

    @Column(name = "introduce")
    private String introduce;

    @Column(name = "detail")
    private String detail;

    @Column(name = "thumb")
    private String thumb;

    @Column(name = "backgroud")
    private String backgroud;

    @Column(name = "isAdv")
    private boolean isAdv;

    @Column(name = "hot")
    private short hot;

    public void setPerfId(int perfId) {
        this.perfId = perfId;
    }

    public int getPerfId() {
        return perfId;
    }

    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }

    public int getSiteId() {
        return siteId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setType(ArrangementType type) {
        this.type = type;
    }

    public ArrangementType getType() {
        return type;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setState(ArrangementState state) {
        this.state = state;
    }

    public ArrangementState getState() {
        return state;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDetail() {
        return detail;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getThumb() {
        return thumb;
    }

    public void setBackgroud(String backgroud) {
        this.backgroud = backgroud;
    }

    public String getBackgroud() {
        return backgroud;
    }

    public void setIsAdv(boolean isAdv) {
        this.isAdv = isAdv;
    }

    public boolean getIsAdv() {
        return isAdv;
    }

    public void setHot(short hot) {
        this.hot = hot;
    }

    public short getHot() {
        return hot;
    }
}

