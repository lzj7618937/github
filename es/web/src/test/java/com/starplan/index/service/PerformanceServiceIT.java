package com.starplan.index.service;

import com.sishuok.es.test.BaseIT;
import com.starplan.index.entity.StarplanPerformance;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.test.context.transaction.TransactionConfiguration;

/**
 * <p>User: jaylee
 * <p>Date: 2015-10-27 上午7:53
 * <p>Version: 1.0
 */
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class PerformanceServiceIT extends BaseIT {
    @Autowired
    private StarplanPerformanceService starplanPerformanceService;

    @Test
    public void testCount() {
        long count = starplanPerformanceService.count();
        Assert.assertEquals(count, 10);
    }

    @Test
    public void testGetPerformances(){
        Page<StarplanPerformance> page = starplanPerformanceService.getPerformances(1,"hot");

        Assert.assertEquals(true, page.isFirstPage());
        Assert.assertEquals(false,page.isLastPage());
        Assert.assertEquals(4,page.getTotalPages());
        Assert.assertEquals(page.getNumberOfElements(),3);
        Assert.assertEquals(page.getTotalElements(),10);
    }

    protected StarplanPerformance createPerformance(){
        StarplanPerformance starplanPerformance = new StarplanPerformance();
        return starplanPerformance;
    }
}
