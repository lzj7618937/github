package com.starplan.index.service;

import com.sishuok.es.common.service.BaseService;


import com.starplan.index.entity.StarplanTicket;
import org.springframework.stereotype.Service;

/**
* starplan_ticket 服务类 
* <p>User: jaylee 
* <p>Date: Tue Oct 27 22:13:11 CST 2015 
* <p>Version: 1.0 
*/ 
@Service
public class StarplanTicketService extends BaseService<StarplanTicket, Long> {
}

