package com.sishuok.es.index.entity;

import com.sishuok.es.common.entity.BaseEntity;

import javax.persistence.*;

/**
 * index_advertisements 实体类
 * <p>User: jaylee
 * <p>Date: Tue Oct 06 11:43:19 CST 2015
 * <p>Version: 1.0
 */

@Entity
@Table(name = "index_advertisements")
public class IndexAdvertisements extends BaseEntity<Long>{

	@Column(name = "available")
	private boolean available;

	@Column(name = "displayorder")
	private byte displayorder;

	@Column(name = "title")
	private String title;

	@Column(name = "targets")
	private String targets;

	@Column(name = "img")
	private String img;

	@Column(name = "comments")
	private String comments;

	public void setAvailable(boolean available){
		this.available = available;
	}

	public boolean getAvailable(){
		return available;
	}

	public void setDisplayorder(byte displayorder){
		this.displayorder = displayorder;
	}

	public byte getDisplayorder(){
		return displayorder;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setTargets(String targets){
		this.targets = targets;
	}

	public String getTargets(){
		return targets;
	}

	public void setImg(String img){
		this.img = img;
	}

	public String getImg(){
		return img;
	}

	public void setComments(String comments){
		this.comments = comments;
	}

	public String getComments(){
		return comments;
	}
}

