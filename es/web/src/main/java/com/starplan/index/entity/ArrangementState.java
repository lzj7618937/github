package com.starplan.index.entity;

/**
 * <p>User: jaylee
 * <p>Date: 2015-10-26 上午9:55
 * <p>Version: 1.0
 */
public enum ArrangementState {
    sell("售中"), selled("售完"), selling("待售");
    private final String info;

    private ArrangementState(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }
}
