package com.starplan.index.repository;

import com.sishuok.es.common.repository.BaseRepository;
import com.starplan.index.entity.StarplanTicket;

/**
* starplan_ticket 数据操作类 
* <p>User: jaylee 
* <p>Date: Tue Oct 27 22:13:11 CST 2015 
* <p>Version: 1.0 
*/ 
public interface StarplanTicketRepository extends BaseRepository<StarplanTicket, Long>{
}

