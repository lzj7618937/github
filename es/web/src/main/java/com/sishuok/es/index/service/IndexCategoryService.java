package com.sishuok.es.index.service;

import com.google.common.collect.Maps;
import com.sishuok.es.common.entity.search.Searchable;
import com.sishuok.es.common.service.BaseService;


import com.sishuok.es.index.entity.IndexCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
* index_category 服务类 
* <p>User: jaylee 
* <p>Date: Tue Oct 06 23:11:26 CST 2015 
* <p>Version: 1.0 
*/ 
@Service
public class IndexCategoryService extends BaseService<IndexCategory, Long> {

    public List<IndexCategory> getMostPopCategory(int num){
        List<IndexCategory> indexCategories = new ArrayList<IndexCategory>();

        Sort sort = new Sort(Sort.Direction.DESC, "hot");
        Pageable pageable = new PageRequest(0, num, sort);

        Page<IndexCategory> page = findAll(pageable);

        if (page.hasContent()) {
            return page.getContent();
        }
        return null;
    }
}

