package com.sishuok.es.index.service;

import com.sishuok.es.index.entity.IndexAdvertisements;
import com.sishuok.es.index.entity.IndexCategory;
import com.sishuok.es.test.BaseIT;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;

import java.util.List;

/**
 * <p>User: jaylee
 * <p>Date: 2015-10-02 上午9:56
 * <p>Version: 1.0
 */
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class ExamIndexServiceIT extends BaseIT {
    @Autowired
    private IndexAdvertisementsService indexAdvertisementsService;

    @Autowired
    private IndexCategoryService indexCategoryService;

    @Test
    public void testGetMostPopAdv(){
        List<IndexAdvertisements> indexAdvertisements = indexAdvertisementsService.getMostPopAdv(3);
        for (IndexAdvertisements indexAdvertisements1:indexAdvertisements){
            System.out.println(indexAdvertisements1.getTitle());
        }

        //indexImageService.findAll();
    }

    @Test
    public void testGetMostPopCategory(){
        List<IndexCategory> indexCategories = indexCategoryService.getMostPopCategory(7);
        for(IndexCategory indexCategory:indexCategories){
            System.out.println(indexCategory.getName() + "  " + indexCategory.getImg());
        }
    }
}
