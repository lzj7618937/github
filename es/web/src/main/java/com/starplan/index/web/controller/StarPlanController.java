package com.starplan.index.web.controller;

import com.sishuok.es.common.entity.search.Searchable;
import com.sishuok.es.common.web.bind.annotation.PageableDefaults;
import com.sishuok.es.sys.user.entity.User;
import com.sishuok.es.sys.user.web.bind.annotation.CurrentUser;
import com.starplan.index.service.StarplanPerformanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * <p>User: jaylee
 * <p>Date: 2015-10-28 下午5:44
 * <p>Version: 1.0
 * #                       _oo0oo_                     #
 * #                      o8888888o                    #
 * #                      88" . "88                    #
 * #                      (| -_- |)                    #
 * #                      0\  =  /0                    #
 * #                    ___/`---'\___                  #
 * #                  .' \\|     |# '.                 #
 * #                 / \\|||  :  |||# \                #
 * #                / _||||| -:- |||||- \              #
 * #               |   | \\\  -  #/ |   |              #
 * #               | \_|  ''\---/''  |_/ |             #
 * #               \  .-\__  '-'  ___/-. /             #
 * #             ___'. .'  /--.--\  `. .'___           #
 * #          ."" '<  `.___\_<|>_/___.' >' "".         #
 * #         | | :  `- \`.;`\ _ /`;.`/ - ` : | |       #
 * #         \  \ `_.   \_ __\ /__ _/   .-` /  /       #
 * #     =====`-.____`.___ \_____/___.-`___.-'=====    #
 * #                       `=---='                     #
 * #     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~   #
 * #                                                   #
 * #               佛祖保佑         永无BUG！            #
 * #                       ：佛曰：                     #
 * #             写字楼里写字间,写字间里程序员；           #
 * #             程序人员写程序,又拿程序换酒钱。           #
 * #             酒醒只在网上坐,酒醉还来网下眠；           #
 * #             酒醉酒醒日复日,网上网下年复年。           #
 * #             但愿老死电脑间,不愿鞠躬老板前；           #
 * #             奔驰宝马贵者趣,公交自行程序员。           #
 * #             别人笑我忒疯癫,我笑自己命太贱；           #
 * #             不见满街漂亮妹,哪个归得程序员？           #
 * #    Copyright (c) 2015，mr.lizhengjie@gmail.com    #
 */
@Controller("starPlanController")
@RequestMapping("/starplan")
public class StarPlanController {

    @Autowired
    private StarplanPerformanceService starplanPerformanceService;

    @RequestMapping(value = "index", method = RequestMethod.GET)
    @PageableDefaults(value = Integer.MAX_VALUE, sort = "id=desc")
    public String index(Model model) {
        model.addAttribute("page", starplanPerformanceService.getPerformances(1, "hot"));
        return "starplan/index";
    }
}
